<?php
/*
Copyright (c) 2016, Kristián Feldsam, FELDSAM s.r.o. <info@feldsam.cz>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

	Form Definition

	Tabledefinition

	Datatypes:
	- INTEGER (Forces the input to Int)
	- DOUBLE
	- CURRENCY (Formats the values to currency notation)
	- VARCHAR (no format check, maxlength: 255)
	- TEXT (no format check)
	- DATE (Dateformat, automatic conversion to timestamps)

	Formtype:
	- TEXT (Textfield)
	- TEXTAREA (Textarea)
	- PASSWORD (Password textfield, input is not shown when edited)
	- SELECT (Select option field)
	- RADIO
	- CHECKBOX
	- CHECKBOXARRAY
	- FILE

	VALUE:
	- Wert oder Array

	Hint:
	The ID field of the database table is not part of the datafield definition.
	The ID field must be always auto incement (int or bigint).

	Search:
	- searchable = 1 or searchable = 2 include the field in the search
	- searchable = 1: this field will be the title of the search result
	- searchable = 2: this field will be included in the description of the search result


*/

$form["title"]    = "New service wizard";
$form["description"]  = "";
$form["name"]    = "new_service";
$form["action"]   = "new_service.php";
$form["db_table"]  = "wizard_template";
$form["db_table_idx"] = "template_id";
$form["db_history"]  = "no";
$form["tab_default"] = "client";
$form["auth"]   = 'yes'; // yes / no

$form["auth_preset"]["userid"]  = 0; // 0 = id of the user, > 0 id must match with id of current user
$form["auth_preset"]["groupid"] = 0; // 0 = default groupid of the user, > 0 id must match with groupid of current user
$form["auth_preset"]["perm_user"] = 'riud'; //r = read, i = insert, u = update, d = delete
$form["auth_preset"]["perm_group"] = 'riud'; //r = read, i = insert, u = update, d = delete
$form["auth_preset"]["perm_other"] = ''; //r = read, i = insert, u = update, d = delete

$form["tabs"]['client'] = array(
	'title'  => "New service",
	'description' => 'Create domain, mail domain, site, database and ftp/ssh',
	'width'  => 100,
	'template'  => "templates/new_service.htm",
	'fields'  => array (
		'domain' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'TEXT',
			'filters'   => array(
				0 => array( 'event' => 'SAVE',
					'type' => 'IDNTOASCII'),
				1 => array( 'event' => 'SHOW',
					'type' => 'IDNTOUTF8'),
				2 => array( 'event' => 'SAVE',
					'type' => 'TOLOWER')
			),
			'validators' => array (
				0 => array ( 'type' => 'NOTEMPTY',
					'errmsg'=> 'domain_error_empty'),
				2 => array ( 'type' => 'REGEX',
					'regex' => '/^[\w\.\-]{2,255}\.[a-zA-Z0-9\-]{2,30}$/',
					'errmsg'=> 'domain_error_regex'),
			),
			'default' => '',
			'value'  => '',
			'width'  => '30',
			'maxlength' => '255'
		),
		'client_id' => array (
			'datatype' => 'INTEGER',
			'formtype' => 'SELECT',
			'default' => '',
			'datasource' => array (  'type' => 'SQL',
				'querystring' => "SELECT sys_group.groupid, sys_group.client_id, sys_group.name, CONCAT(IF(client.company_name != '', CONCAT(client.company_name, ' :: '), ''), client.contact_name, ' (', client.username, IF(client.customer_no != '', CONCAT(', ', client.customer_no), ''), ')') as contactname FROM sys_group, client WHERE sys_group.client_id = client.client_id AND sys_group.client_id > 0 ORDER BY client.company_name, client.contact_name, sys_group.name",
				'keyfield'=> 'client_id',
				'valuefield'=> 'contactname'
			),
			'value'  => ''
		),
		'template_id' => array(
			'datatype' => 'INTEGER',
			'formtype' => 'SELECT',
			'default' => '',
			'datasource' => array (  'type' => 'SQL',
				'querystring' => 'SELECT template_id, template_name FROM wizard_template WHERE {AUTHSQL} ORDER BY template_name',
				'keyfield'=> 'template_id',
				'valuefield'=> 'template_name'
			),
			'value'  => '',	
		),
		'db' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'CHECKBOX',
			'default' => 'y',
			'value'  => array(0 => 'n', 1 => 'y')
		),
		'db_type' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'SELECT',
			'default' => 'mysql',
			'value'  => array(
				//'mongo' => 'MongoDB',
				'mysql' => 'MySQL'
			)
		),
		'ftp' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'CHECKBOX',
			'default' => 'y',
			'value'  => array(0 => 'n', 1 => 'y')
		),
		'ssh' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'CHECKBOX',
			'default' => 'y',
			'value'  => array(0 => 'n', 1 => 'y')
		),
		'mail' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'CHECKBOX',
			'default' => 'y',
			'value'  => array(0 => 'n', 1 => 'y')
		),
		'mailboxes' => array (
			'datatype' => 'TEXT',
			'formtype' => 'TEXTAREA',
			'default' => '',
			'value'  => '',
			'width'  => '30',
		),
		'dkim' => array (
			'datatype'      => 'VARCHAR',
			'formtype'      => 'CHECKBOX',
			'default'       => 'n',
			'value'         => array(0 => 'n', 1 => 'y')
		),
		'dkim_private' => array (
			'datatype'      => 'TEXT',
			'formtype'      => 'TEXTAREA',
			'default'       => '',
			'value'         => '',
			'cols'          => '30',
			'rows'          => '10',
			'validators'    => array (  0 => array ('type'  => 'CUSTOM',
					'class' => 'validate_dkim',
					'function' => 'check_private_key',
					'errmsg'=> 'dkim_private_key_error'),
			),
		),
		'dkim_public' => array (
			'datatype'      => 'TEXT',
			'formtype'      => 'TEXTAREA',
			'default'       => '',
			'value'         => '',
			'cols'          => '30',
			'rows'          => '10'
		),
		'dkim_selector' => array (
			'datatype'      => 'VARCHAR',
			'formtype'      => 'TEXT',
			'default'       => 'default',
			'value'         => 'default',
			'width'  => '20',
			'maxlength' => '63',
			'validators' => array (  0 => array (   'type' => 'REGEX',
					'regex' => '/^[a-z0-9]{0,63}$/',
					'errmsg'=> 'dkim_selector_error'),
			),
		),
		'dns' => array (
			'datatype' => 'VARCHAR',
			'formtype' => 'CHECKBOX',
			'default' => 'y',
			'value'  => array(0 => 'n', 1 => 'y')
		),
		'dns_template_id' => array(
			'datatype' => 'INTEGER',
			'formtype' => 'SELECT',
			'default' => '',
			'datasource' => array (  'type' => 'SQL',
				'querystring' => "SELECT template_id, name FROM dns_template WHERE visible = 'Y' ORDER BY name ASC",
				'keyfield'=> 'template_id',
				'valuefield'=> 'name'
			),
			'value'  => '',	
		),
	)
);