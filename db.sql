/*
Copyright (c) 2016, Kristián Feldsam, FELDSAM s.r.o. <info@feldsam.cz>
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice,
      this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice,
      this list of conditions and the following disclaimer in the documentation
      and/or other materials provided with the distribution.
    * Neither the name of ISPConfig nor the names of its contributors
      may be used to endorse or promote products derived from this software without
      specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

USE `dbispconfig`;

CREATE TABLE `wizard_template` (
  `template_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_userid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_groupid` int(11) unsigned NOT NULL DEFAULT '0',
  `sys_perm_user` varchar(5) DEFAULT NULL,
  `sys_perm_group` varchar(5) DEFAULT NULL,
  `sys_perm_other` varchar(5) DEFAULT NULL,
  `web_server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `mail_server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `database_server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `dns_server_id` int(11) unsigned NOT NULL DEFAULT '0',
  `template_name` varchar(255) NOT NULL DEFAULT '',
  `ip_address` varchar(39) DEFAULT NULL,
  `ipv6_address` varchar(255) DEFAULT NULL,
  `traffic_quota` bigint(20) NOT NULL DEFAULT '-1',
  `hd_quota` bigint(20) NOT NULL DEFAULT '-1',
  `database_quota` int(11) unsigned DEFAULT NULL,
  `mailbox_quota` int(11) unsigned DEFAULT NULL,
  `cgi` enum('n','y') NOT NULL DEFAULT 'n',
  `ssi` enum('n','y') NOT NULL DEFAULT 'n',
  `suexec` enum('n','y') NOT NULL DEFAULT 'y',
  `errordocs` tinyint(1) NOT NULL DEFAULT '1',
  `subdomain` enum('none','www','*') NOT NULL DEFAULT 'www',
  `php` varchar(32) NOT NULL DEFAULT 'fast-cgi',
  `fastcgi_php_version` varchar(255) DEFAULT NULL,
  `ruby` enum('n','y') NOT NULL DEFAULT 'n',
  `python` enum('n','y') NOT NULL DEFAULT 'n',
  `perl` enum('n','y') NOT NULL DEFAULT 'n',
  `seo_redirect` varchar(255) DEFAULT NULL,
  `rewrite_to_https` enum('n','y') NOT NULL DEFAULT 'n',
  `allow_override` varchar(255) NOT NULL DEFAULT 'All',
  `http_port` int(11) NOT NULL DEFAULT '80',
  `https_port` int(11) NOT NULL DEFAULT '443',
  `log_retention` int(11) NOT NULL DEFAULT '30',
  `apache_directives` mediumtext,
  `nginx_directives` mediumtext,
  `php_fpm_use_socket` enum('n','y') NOT NULL DEFAULT 'n',
  `pm` enum('static','dynamic','ondemand') NOT NULL DEFAULT 'dynamic',
  `pm_max_children` int(11) unsigned NOT NULL DEFAULT '10',
  `pm_start_servers` int(11) unsigned NOT NULL DEFAULT '2',
  `pm_min_spare_servers` int(11) unsigned NOT NULL DEFAULT '1',
  `pm_max_spare_servers` int(11) unsigned NOT NULL DEFAULT '5',
  `pm_process_idle_timeout` int(11) unsigned NOT NULL DEFAULT '10',
  `pm_max_requests` int(11) unsigned NOT NULL DEFAULT '0',
  `custom_php_ini` mediumtext,
  `database_charset` varchar(64) DEFAULT NULL,
  `database_remote_access` enum('n','y') NOT NULL DEFAULT 'n',
  `database_remote_ips` text,
  `policy` int(11) unsigned NOT NULL DEFAULT '0',
  `quota_files` bigint(20) NOT NULL DEFAULT '-1',
  `ul_ratio` int(11) NOT NULL DEFAULT '-1',
  `dl_ratio` int(11) NOT NULL DEFAULT '-1',
  `ul_bandwidth` int(11) NOT NULL DEFAULT '-1',
  `dl_bandwidth` int(11) NOT NULL DEFAULT '-1',
  `shell` varchar(255) NOT NULL DEFAULT '/bin/bash',
  `shell_chroot` varchar(255) NOT NULL DEFAULT '',
  `enablesmtp` enum('n','y') NOT NULL DEFAULT 'y',
  `enableimap` enum('n','y') NOT NULL DEFAULT 'y',
  `enablepop3` enum('n','y') NOT NULL DEFAULT 'y',
  PRIMARY KEY (`template_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
